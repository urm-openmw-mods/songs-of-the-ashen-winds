# Songs of the Ashen Winds

This mod includes a few melodies composed to the words of Ashlander poetry from the original game.

1. Ashland Hymns: Wondrous Love `bk_ashland_hymns`
2. The Five Far Stars: Red Mountain `bk_five_far_stars`
3. Words of the Wind: May I shrink to Dust `bk_words_of_the_wind`

By default, the track plays when you open the matching book, and stops when you stop reading it.

Requires a recent [development build of OpenMW](https://openmw.org/downloads/).

To install, read the documentation on
[how to install OpenMW mods](https://openmw.readthedocs.io/en/latest/reference/modding/mod-install.html?highlight=installing%20mods)
and [how to enable OpenMW scripts](https://openmw.readthedocs.io/en/latest/reference/lua-scripting/overview.html#how-to-run-a-script)


[![Video](./upscaled_logo.png)](https://youtu.be/KWDE4UCiKTc)
